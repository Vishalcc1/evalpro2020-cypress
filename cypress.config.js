const { defineConfig } = require('cypress')
module.exports = defineConfig({
  viewportHeight:860,
  viewportWidth:1500,
  "reporter": "cypress-mochawesome-reporter",
  "reporterOptions": {
    "reportDir": "cypress/reports",
    "charts": true,
    "reportPageTitle": "My Test Suite",
    "embeddedScreenshots": true,
    "inlineAssets": true
  },
  "video": false,
  "retries": {
    // Configure retry attempts for `cypress run`
    // Default is 0
    "runMode": 0,
    // Configure retry attempts for `cypress open`
    // Default is 0
    "openMode": 0
  },
  defaultCommandTimeout: 20000,
  pageLoadTimeout: 20000000,
  redirectionLimit:5000,
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config)
    },
    baseUrl: 'https://hansa.bodhi.cse.iitb.ac.in/',
  },
})
