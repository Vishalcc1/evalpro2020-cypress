/// <reference types="cypress" />$

// all webpage need to create course,assignment and add student to Course

import loginPage from '../../../pages/loginPage'
import dashboardPage from '../../../pages/dashboardPage'
import exploreCourses from '../../../pages/exploreCoursesPage'
import CoursePage from '../../../pages/coursePage'
import instructorLabPageCreateBasicAssignemt from '../../../pages/instructorLabPageCreateBasicAssignment'
import instructorLabPageDashboard from '../../../pages/instructorLabPageDashboard'
import importAssignmentPage from '../../../pages/importAssignmentPage'
import instructorLabListPage from '../../../pages/instructorLabListPage'
import instructorAddCoursePopUp from '../../../pages/instructorAddCoursePopUp.js'




const student_list = require('../../../fixtures/test_json/common/students.json')
const instructur_list = require('../../../fixtures/test_json/common/instructors.json')


const create_course = require('../../../fixtures/test_json/create_course.json')
const create_assignment = require('../../../fixtures/test_json/import_all_assignment.json')


Cypress.on('uncaught:exception', (err, runnable) => {
    console.log('error', err)
    console.log('runnable', runnable)
    cy.now('log', 'caught error', err)
    return false
  })
 

  describe('Instructor-Creates-Course', () => {
    
    it('Instructor Creating a Course and Publishing It', () => {
        cy.visit('/accounts/login/')
        const instructor = instructur_list.instructor1

        //Login as Instructors
        loginPage.typeUsername(instructor.username)
        loginPage.typePassword(instructor.password)
        loginPage.clickLogin();


        dashboardPage.checkIfItsDashboard()
        dashboardPage.selectRole('Instructor')

        //cy.pause()

        dashboardPage.clickCreateCourseBtn()

        instructorAddCoursePopUp.typeCourseCode(create_course['course-code'])
        instructorAddCoursePopUp.typeCourseName(create_course['course-name'])
        
        instructorAddCoursePopUp.clickAddCourse()

        cy.log(create_course['course-name'])

        dashboardPage.clickPublish_Unpublish_Course(create_course['course-name'])
        dashboardPage.signOut()
        Cypress.on('uncaught:exception', (err, runnable) => {
            console.log('error', err)
            console.log('runnable', runnable)
            cy.now('log', 'caught error', err)
            return false
          })
    })


    it('Student Registering the Created Course', () => {
        cy.visit('/accounts/login/')
        const student = student_list.student1

        //Login as Instructors
        loginPage.typeUsername(student.username)
        loginPage.typePassword(student.password)
        loginPage.clickLogin();


        dashboardPage.checkIfItsDashboard()

        cy.wait(2000)
        dashboardPage.selectRole('Student')

        dashboardPage.clickExploreCourses()
        exploreCourses.clickCourse(create_course['course-name'])
        exploreCourses.clickEnrollBtn()

        cy.wait(2000)
        dashboardPage.clickMyDashboard()
        
        //dashboardPage.unregisterCourse(create_course['course-name'])
        //dashboardPage.clickUnregisterYes()

        dashboardPage.signOut()
        Cypress.on('uncaught:exception', (err, runnable) => {
            console.log('error', err)
            console.log('runnable', runnable)
            cy.now('log', 'caught error', err)
            return false
          })
    })
    

  })

describe('Instructor-Creates-Assignment', () => {
    it('Creating a Assignment', () => {
        cy.visit('/accounts/login/')
        const instructor = instructur_list.instructor1

        //Login as Instructors
        loginPage.typeUsername(instructor.username)
        loginPage.typePassword(instructor.password)
        loginPage.clickLogin();


        dashboardPage.checkIfItsDashboard()
        dashboardPage.selectRole('Instructor')
        exploreCourses.clickCourse(create_course['course-name'])
        CoursePage.sidebar.clickProgrammingLabs()
        
        
        
        create_assignment.assignmentList.forEach( assignment_details => {
            instructorLabPageCreateBasicAssignemt.clickImportButton()
            importAssignmentPage.uploadTestFile(assignment_details.assignmentPath, 
                assignment_details.mimeType  ,assignment_details.assigmentName)    
            
            importAssignmentPage.gotoAssignmentDescriptionUsingBreadCrumb("All Assignments")
            instructorLabPageDashboard.clickUnhideButton(assignment_details.LabName)
            
    });

    })

    // it('Edit a Assignment Name Output is case sensitive', () => {
    //     cy.visit('/accounts/login/')
    //     const instructor = instructur_list.instructor5

    //     //Login as Instructors
    //     loginPage.typeUsername(instructor.username)
    //     loginPage.typePassword(instructor.password)
    //     loginPage.clickLogin();


    //     dashboardPage.checkIfItsDashboard()
    //     dashboardPage.selectRole('Instructor')
    //     exploreCourses.clickCourse(create_course['course-name'])
    //     CoursePage.sidebar.clickProgrammingLabs()
        
    //     instructorLabListPage.gotoLab("output_is_case_sensitive")
        
    //     instructorLabPageDashboard.editAssignment()
        
    //     instructorLabPageDashboard.programmingDeatilsTab()

    //     instructorLabPageDashboard.featureOutputCaseSensitiveClkFun()

    //     instructorLabPageDashboard.saveAndCloseClkFun()

    // })

    // it('Edit a Assignment Name Output contain Number Only', () => {
    //     cy.visit('/accounts/login/')
    //     const instructor = instructur_list.instructor5

    //     //Login as Instructors
    //     loginPage.typeUsername(instructor.username)
    //     loginPage.typePassword(instructor.password)
    //     loginPage.clickLogin();


    //     dashboardPage.checkIfItsDashboard()
    //     dashboardPage.selectRole('Instructor')
    //     exploreCourses.clickCourse(create_course['course-name'])
    //     CoursePage.sidebar.clickProgrammingLabs()
        
    //     instructorLabListPage.gotoLab("output_contains_numbers_only")
        
    //     instructorLabPageDashboard.editAssignment()
        
    //     instructorLabPageDashboard.programmingDeatilsTab()

    //     instructorLabPageDashboard.featureOutputContainNumberClkFun()
        

    // })

    // it('Edit a Assignment Name Ignore White Space', () => {
    //     cy.visit('/accounts/login/')
    //     const instructor = instructur_list.instructor5

    //     //Login as Instructors
    //     loginPage.typeUsername(instructor.username)
    //     loginPage.typePassword(instructor.password)
    //     loginPage.clickLogin();


    //     dashboardPage.checkIfItsDashboard()
    //     dashboardPage.selectRole('Instructor')
    //     exploreCourses.clickCourse(create_course['course-name'])
    //     CoursePage.sidebar.clickProgrammingLabs()
        
    //     instructorLabListPage.gotoLab("ignore_extra_whitespaces")
        
    //     instructorLabPageDashboard.editAssignment()
        
    //     instructorLabPageDashboard.programmingDeatilsTab()

    //     instructorLabPageDashboard.featureIgnoreExtraSpaceClkFun()

    //     instructorLabPageDashboard.saveAndCloseClkFun()

    // })

    // it('Edit a Assignment Name Allow Multiple Delimeter', () => {
    //     cy.visit('/accounts/login/')
    //     const instructor = instructur_list.instructor5

    //     //Login as Instructors
    //     loginPage.typeUsername(instructor.username)
    //     loginPage.typePassword(instructor.password)
    //     loginPage.clickLogin();


    //     dashboardPage.checkIfItsDashboard()
    //     dashboardPage.selectRole('Instructor')
    //     exploreCourses.clickCourse(create_course['course-name'])
    //     CoursePage.sidebar.clickProgrammingLabs()
        
    //     instructorLabListPage.gotoLab("allow_multiple_delimeter")
        
    //     instructorLabPageDashboard.editAssignment()
        
    //     instructorLabPageDashboard.programmingDeatilsTab()

    //     instructorLabPageDashboard.featureMultipleDelimeterClk()
        
    //     instructorLabPageDashboard.delimeterCommaClkFun()
    //     instructorLabPageDashboard.delimeterPipeClkFun()
    //     instructorLabPageDashboard.delimeterSemiCommaClkFun()
    //     instructorLabPageDashboard.delimeterSpaceClkFun()
    //     instructorLabPageDashboard.delimeterTabClkFun()

    //     instructorLabPageDashboard.saveAndCloseClkFun()

    // })


})
