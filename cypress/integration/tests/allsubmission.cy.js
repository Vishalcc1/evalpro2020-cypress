/// <reference types="cypress" />$

import loginPage from '../../pages/loginPage'
import dashboardPage from '../../pages/dashboardPage'
import CoursePage from '../../pages/coursePage'
import StudentLabsListPage from '../../pages/studentLabsListPage'
import StudentLabPage from '../../pages/studentLabPage'
import Editor from '../../pages/editor'

const student_login = require('../../fixtures/test_json/common/students.json')
const lab_details = require('../../fixtures/test_json/submission_tests.json')

Cypress.on('uncaught:exception', (err, runnable) => {
    console.log('error', err)
    console.log('runnable', runnable)
    cy.now('log', 'caught error', err)
    return false
  })



describe('Testing assignment submission functionality', () => {
    it('Editor', () => {
        cy.visit('/accounts/login/')
        const student = student_login.student1
        loginPage.typeUsername(student.username)
        loginPage.typePassword(student.password)
        loginPage.clickLogin();

        dashboardPage.checkIfItsDashboard()
        cy.wait(2000)
        dashboardPage.selectRole('Student')
        dashboardPage.clickCourse(lab_details['course-name'])
        CoursePage.sidebar.clickProgrammingLabs()


        

        lab_details.labList.forEach(lab => {

            StudentLabsListPage.gotoLab(lab['programName'])

            lab.Testcodes.forEach(testcode => {
        
                cy.log(testcode.objective)
                StudentLabPage.clickEditorLink()
                //Editor.clickFullScreen()

                Editor.typeEditor('{ctrl+a}{del}',true)


                cy.readFile(testcode['codefilepath']).then((code) => {

                    //Editor.alternateType("{Hello}")
                    const codeLines = code.split("\n");

                    codeLines.forEach(line => {
                        if(line != ''){
                            Editor.typeEditor('{home}',true)
                            //cy.pause()
                            Editor.typeEditor(line+' ',false)
                        }
                        Editor.typeEditor('{end}{enter}',true)
                        //cy.pause()
                    
                    });
                })


                Editor.clickSave()

                testcode['Testcases'].forEach(test => {
                    Editor.selectActionSubmitAndPractice()
                    Editor.verifyTestcaseTableRow(test.Name,test.Status,test.Marks)
                    Editor.clickCompare(test.Name)
                    //Editor.verifyTestcaseComapare(test['Error-message'])
                    Editor.clickBrowserBackButtton()
                });

                // cy.pause()

                Editor.gotoAssignmentDescriptionUsingBreadCrumb(lab['programName'])


            });
            CoursePage.sidebar.clickProgrammingLabs()

        })
        //StudentLabPage.clickEditorLink()



    })
})
