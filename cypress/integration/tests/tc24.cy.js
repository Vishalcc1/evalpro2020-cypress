/// <reference types="cypress" />$

import loginPage from '../../pages/loginPage'
import dashboardPage from '../../pages/dashboardPage'
import CoursePage from '../../pages/coursePage'
import StudentLabsListPage from '../../pages/studentLabsListPage'
import StudentLabPage from '../../pages/studentLabPage'
import Editor from '../../pages/editor'

const student_login = require('../../fixtures/test_json/common/students.json')
const lab_details = require('../../fixtures/test_json/submission_tests.json')

Cypress.on('uncaught:exception', (err, runnable) => {
    console.log('error', err)
    console.log('runnable', runnable)
    cy.now('log', 'caught error', err)
    return false
  })



describe('Testing assignment submission functionality', () => {
    var lab = lab_details.labList[23]
    lab.Testcodes.forEach(testcode => {
        it(lab.programName, () => {
            cy.visit('/accounts/login/')
            const student = student_login.student1
            loginPage.typeUsername(student.username)
            loginPage.typePassword(student.password)
            loginPage.clickLogin();
    
            dashboardPage.checkIfItsDashboard()
            cy.wait(2000)
            dashboardPage.selectRole('Student')
            dashboardPage.clickCourse(lab_details['course-name'])
            CoursePage.sidebar.clickProgrammingLabs()
            StudentLabsListPage.gotoLab(lab['programName'])
    
                
            
            cy.log(testcode.objective)
            StudentLabPage.uploadAssignmentFile(testcode.codefilepath,testcode['mime-type'],testcode.filename)
            StudentLabPage.clickEditorLink()
    
            Editor.clickSave()
    
            testcode['Testcases'].forEach(test => {
                Editor.selectActionSubmitAndPractice()
                Editor.verifyTestcaseTableRow(test.Name,test.Status,test.Marks)
                Editor.clickCompare(test.Name)
                //Editor.verifyTestcaseComapare(test['Error-message'])
                Editor.clickBrowserBackButtton()
            });
    
    
        })
    });
    })
    