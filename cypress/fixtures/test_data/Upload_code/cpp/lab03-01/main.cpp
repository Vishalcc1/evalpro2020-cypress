#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

int main(){
    int n=1, p=1;
    cin >> n;
    cin >> p;

    vector <int> u1;
    vector <int> u2;
    int element;
    for(int i=0; i<n; i++){
        cin >> element;
        u1.push_back(element);
        cin >> element;
        u2.push_back(element);
    }

    int modular_dist=0;
    for(int i=0; i<n; i++){
        modular_dist = modular_dist + pow(abs(u1.at(i)-u2.at(i)), p);
    }

    double power = 1/(double)p;
    double result = pow((double)modular_dist, power);
    cout << fixed;               
    cout.precision(2);
    cout << result;

};
