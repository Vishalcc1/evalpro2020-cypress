//logical error zero marks except unique number in list because order does not matter

#include<bits/stdc++.h>

#include <random>
using namespace std;

int random(int min, int max) //range : [min, max]
{
   static bool first = true;
   if (first) 
   {  
      srand( time(NULL) ); //seeding for the first time only!
      first = false;
   }
   return min + rand() % (( max + 1 ) - min);
}

int main()
{
// correct code
std::random_device rd; // obtain a random number from hardware
std::mt19937 gen(rd()); // seed the generator
// logical incorrect code because order does not matter
int n,s=1;
vector<int> arr;
cin>>n;
for(int i=0;i<n;i++){

for(int j=0;j<=i;j++){
	arr.push_back(s++);
}
for(int j=arr.size()-1;j>=0;j--){
	cout<<arr[j]+random(-1,1)<<" ";
}
arr.resize(0);

cout<<"\n";
}
}
