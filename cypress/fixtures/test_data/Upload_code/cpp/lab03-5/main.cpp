#include <iostream>
#include <vector>
#include <random>
#include <ctime>
#include <cmath>
using namespace std;
//logical correct
int main()
{
  int n, p;
  cin>>n>>p;
  double res = 0; 
  while(n--){
    int u, v;
    cin>>u>>v;
    res += pow(abs(u-v),p);

  }
  double ans = pow(res,1.0/p); 
  cout<<fixed;
  cout.precision(2);
  cout<<ans<<"\n";

}
