
#include<bits/stdc++.h>

#include <random>
using namespace std;

int random(int min, int max) //range : [min, max]
{
   static bool first = true;
   if (first) 
   {  
      srand( time(NULL) ); //seeding for the first time only!
      first = false;
   }
   return min + rand() % (( max + 1 ) - min);
}

int rec(int n){
	if(n<2)return 1;
	else 
	return n*rec(n-1);
}

int main()
{
int n;
cin>>n;
cout<<rec(n);
}
