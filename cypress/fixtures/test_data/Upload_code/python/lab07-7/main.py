import os


# Create a child process
# using os.fork() method
pid = os.fork()

if(pid==0):
    # pid greater than 0 represents
# the parent process
    n =int(input())
    s=1
    for i in range(n):
        for j in range(i+1):
            print(s,end=" ")
            s=s+1
        print("\n")