import os
import random


# Create a child process
# using os.fork() method
pid = os.fork()

# pid greater than 0 represents
# the parent process
if(pid==0):
    n=int(input())
    for i in range(n):
        for j in range(n):
            print("*",end=" ")
        print("\n")
        for j in range(n-1):
            print(" *",end="")
        print("\n")
