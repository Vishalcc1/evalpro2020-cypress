// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import 'cypress-file-upload';
import loginPage from '../pages/loginPage'
import dashboardPage from '../pages/dashboardPage'
import CoursePage from '../pages/coursePage'
import StudentLabsListPage from '../pages/studentLabsListPage'
import StudentLabPage from '../pages/studentLabPage'
import 'cypress-file-upload'

const students = require('../fixtures/test_json/common/students.json')
const instructors = require('../fixtures/test_json/common/instructors.json')

Cypress.Commands.add('getUser', (username) => {
    if(username in students)
        return students[username]
    else if(username in instructors)
        return instructors[username]
    return undefined
})

Cypress.Commands.add('navigateLoginToUpload', (cfg) => {
    /**
     * Function which logins with student login, navigates till assignment page
     * and uploads the assignment file. 
     * Utilized by test cases such as submission_tests.json 
     * negativeSubmissionTest-FailByNonPythonFileUpload
     * 
     * @cfg Configuration containing student username, ?password?, course name, assignment 
     *      name, submission file path.
     *      Example: negativeSubmissionTest-FailByNonPythonFileUpload in submission_tests.json 
     */
    loginPage.login(cfg.username, cfg.password)
    dashboardPage.checkIfItsDashboard()
    dashboardPage.clickCourse(cfg.course)
    CoursePage.sidebar.clickProgrammingLabs()
    StudentLabsListPage.gotoLab(cfg.lab)
    // console.log('======>', cfg.assignmentFilePath)
    StudentLabPage.uploadAssignmentFile(cfg.assignmentFilePath, cfg.mimeType, cfg.filename)
})

Cypress.Commands.add('navigateLoginToGoToLab', cfg => {
    loginPage.login(cfg.username, cfg.password)
    dashboardPage.checkIfItsDashboard()
    dashboardPage.clickCourse(cfg.course)
    CoursePage.sidebar.clickProgrammingLabs()
    StudentLabsListPage.gotoLab(cfg.lab)
}) 

Cypress.Commands.add('studentLoginToAssignmentUpload', (cfg) => {
    cy.navigateLoginToGoToLab(cfg)
    StudentLabPage.uploadAssignmentFile(cfg.assignmentFilePath, cfg.mimeType, cfg.filename)
})
// Cypress.Commands.add('createCourse')