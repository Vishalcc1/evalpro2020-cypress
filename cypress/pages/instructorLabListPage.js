import ParentPage from './parentPage'

class StudentLabsListPage extends ParentPage {
    elements = {
        lab: (labName) => cy.contains('a', labName)
    }

    gotoLab(labName) {
        this.elements.lab(labName).click()
    }
}

export default new StudentLabsListPage();