import CourseSidebar from './courseSidebar'

/**
 * On Instructor login.
 * Reach by: Clicking on labs using CourseSidebar.
 */

 class instructorLabPageDashboard {
    elements = {
        labRowInTable : (assName) => cy.contains('div.row',assName),
        editassignClk : () => cy.get('[title="Edit Assignment"] > .glyphicon').click(),
        programmingDeatilsTabClk : () => cy.get('[style="border: 1px solid #ccc;border-radius: 4px;"] > .nav > :nth-child(3) > a').click(),
        featureCalcExeTimeClk : () => cy.get('#togglecontainer').click(),
        featureIgnoreExtraSpaceClk : () => cy.get('#id_ignore_extra_spaces').click(),
        featureOutputContainNumberClk : () => cy.get('#id_correctness').click(),
        featureIndentCodeClk : () => cy.get('#id_indentation').click(),
        featureOutputCaseSensitiveClk : () => cy.get('#id_ignore_case').click(),
        featureMultipleDelimeterClk : () => cy.get('#id_allow_delimeters').click(),
        delimeterCommaClk : () => cy.get('#delimeters > :nth-child(1) > label').click(),
        delimeterSpaceClk : () => cy.get('#space').click(),
        delimeterSemiCommaClk : () => cy.get('#semi-comma').click(),
        delimeterTabClk : () => cy.get('#tab').click(),
        delimeterPipeClk : () => cy.get('#vline').click(),
        saveAndCloseClk : () => cy.get('#id_submit').click()
    }

    saveAndCloseClkFun()
    {
        this.elements.saveAndCloseClk()
    }

    featureCalcExeTimeClkFun()
    {
        this.elements.featureCalcExeTimeClk()
    }
    featureIgnoreExtraSpaceClkFun()
    {
        this.elements.featureIgnoreExtraSpaceClk()
    }
    featureOutputContainNumberClkFun()
    {
        this.elements.featureOutputContainNumberClk()
    }
    featureIndentCodeClkFun()
    {
        this.elements.featureIndentCodeClk()
    }
    featureOutputCaseSensitiveClkFun()
    {
        this.elements.featureOutputCaseSensitiveClk()
    }
    featureMultipleDelimeterClk()
    {
        this.elements.featureMultipleDelimeterClk()
    }
    delimeterCommaClkFun()
    {
        this.elements.delimeterCommaClk()
    }

    delimeterSpaceClkFun()
    {
        this.elements.delimeterSpaceClk()
    }
    delimeterSemiCommaClkFun()
    {
        this.elements.delimeterSemiCommaClk()
    }

    delimeterTabClkFun()
    {
        this.elements.delimeterTabClk()
    }

    delimeterPipeClkFun()
    {
        this.elements.delimeterPipeClk()
    }

    verifyDashboardDetails(assName,submitDeadline){
        this.elements.labRowInTable(assName).within(() => {
            cy.contains('a',assName).should('have.html',assName)
            //cy.get('div span').eq(2).should('have.html','2021-09-06 14:23')
            // cy.get('div span').eq(3).should('have.html',submitDeadline.slice(0,-3))
        })
    }

    clickLabName(assName){
        this.elements.labRowInTable(assName).contains('a',assName).click()
    }

    clickUnhideButton(assName){
        this.elements.labRowInTable(assName).contains('button','unhide').click()
    }

    editAssignment()
    {
        this.elements.editassignClk()
    }

    programmingDeatilsTab()
    {
        this.elements.programmingDeatilsTabClk()

    }


}

export default new instructorLabPageDashboard();