// import parentPage from './parentPage'  //TODO shouldnt header be present on this page too?
import CourseSidebar from './courseSidebar'
const dayjs = require('dayjs')
/**
 * On student login.
 * Reach by: Clicking any of programming labs on course content page.
 * Relative link: <baseUrl>/assignments/details/5
 */
class StudentLabPage {

    sidebar = new CourseSidebar();
    elements = {
        chooseAssignmentFileToUploadBtn : () => cy.get('input[name="docfile"]'),
        uploadBtn : () => cy.contains('input','Upload'),
        runPracticeTestBtn : () => cy.contains('a', 'Run practice test'), 
        editor: () => cy.contains('a','Editor'), //no need to test open source editor control
        submittedAssignmentFileLink: (fileName) => cy.contains('a', fileName),
        fileUploadTime: (fileName) => cy.contains('a',fileName).parent().next(),
        uploadResult: (isExist) =>  cy.contains('ERROR:').should(isExist),
        closeErrorPopup: ()=> cy.get('[class="close"]'),
        downloadSubmittedAssignment:(fileName)=> cy.contains(fileName).click(),
        viewTest: ()=> cy.get(':nth-child(5) > .btn')
        // fileUploadTime: () => cy.get('li > .table > tbody > :nth-child(2) > :nth-child(2)') //.debug()
    }
    openTestView()
    {
        this.elements.viewTest().click()
    }
    downloadLatestFileandCheck(fileName){
        this.elements.downloadSubmittedAssignment(fileName)
        cy.readFile('/home/paras/Desktop/bodhitree-ui-test-automation-master/cypress/downloads/makeSquareOf1.cpp')
        .should('eq',
        '#include<iostream>using namespace std;int main( ){int input;cin>>input;cout<<input*input;return 0;}')
    }
    
    checkUploadResult(isExist)
    {
        this.elements.uploadResult(isExist)
        if(isExist=="exist")
          {
            this.elements.closeErrorPopup().click()
          }
          
    }

    uploadAssignmentFile(_filePath, _mimeType, _fileName) {
        console.log('----->', _filePath, '---' , _mimeType)
        console.log('Uploaded file: =======>', _filePath)
        this.elements.chooseAssignmentFileToUploadBtn().attachFile({ filePath : _filePath, mimeType : _mimeType , fileName :  _fileName})
        this.elements.uploadBtn().click()
    }

    runPracticeTest() {
        //return cy.wrap(this.elements.runPracticeTestBtn().click())
        return this.elements.runPracticeTestBtn().click()
    }

    clickEditorLink() {
        this.elements.editor().click()
    }

    errorOccurred(errorMessage) {
        cy.contains(errorMessage)
    }

    getFileUploadTime2(fileName) {
        // const td =  Cypress.SelectorPlayground.getSelector(this.elements.fileUploadTime(fileName))
        // const td =  this.elements.fileUploadTime(fileName).then(({data}) => {
        //     debugger
        //     const a = 2
        // })
        
        const td = this.elements.fileUploadTime()

        console.log('-->' , td)
        // console.log('---->' , td.invoke('text'))
        return td.invoke('text')
    }

    // getFileUploadTime(fileName) {
    //     const uploadTime = new Cypress.Promise<string>((resolve) => {
    //         this.elements.fileUploadTime().invoke('text')
    //             .then((txt) => resolve(txt.toString()))
    //     })
    //     return uploadTime
    // }

    checkUploadTime(fileName) {
        console.log('---->', fileName)
        this.elements.fileUploadTime(fileName).then(($td) => {
            const uploadDateTimeStr = $td.text().replace('a.m.', 'am').replace('p.m.', 'pm');
            console.log('-->', uploadDateTimeStr)
            const uploadDateTime = dayjs(uploadDateTimeStr, 'MMM. DD, YYYY, HH:MM a') 
            const now = dayjs()

            // console.log('-->', uploadDateTime)
            // console.log('-->', now)
            const diffMinutes = now.minute() - uploadDateTime.minute()
            expect(diffMinutes).to.be.lte(1) 
        })
    }
    
    getUploadTime2(fileName) {
        console.log('fileName:', fileName)
        return cy.wrap(this.elements.fileUploadTime(fileName)) 
        /*
        .then(($td) => {
            const uploadDateTimeStr = $td.text().replace('a.m.', 'am').replace('p.m.', 'pm');
            console.log('uploadDateTimeStr: ', uploadDateTimeStr)
            return cy.wrap(uploadDateTimeStr);
            // const uploadDateTime = dayjs(uploadDateTimeStr, 'MMM. DD, YYYY, HH:MM a') 
            // const now = dayjs()

            // // console.log('-->', uploadDateTime)
            // // console.log('-->', now)
            // const diffMinutes = now.minute() - uploadDateTime.minute()
            // expect(diffMinutes).to.be.lte(1) 
        })
        */
    }

    getUploadTime(fileName) {
        console.log('fileName:', fileName)
        this.elements.fileUploadTime(fileName).then(($td) => {
            const uploadDateTimeStr = $td.text().replace('a.m.', 'am').replace('p.m.', 'pm');
            console.log('uploadDateTimeStr: ', uploadDateTimeStr)
            return cy.wrap(uploadDateTimeStr);
            // const uploadDateTime = dayjs(uploadDateTimeStr, 'MMM. DD, YYYY, HH:MM a') 
            // const now = dayjs()

            // // console.log('-->', uploadDateTime)
            // // console.log('-->', now)
            // const diffMinutes = now.minute() - uploadDateTime.minute()
            // expect(diffMinutes).to.be.lte(1) 
        })
    }
    


    // getFileUploadTime3(fileName) {
    //         this.elements.fileUploadTime()
    //     return uploadTime
    // }

    downloadSubmittedAssignmentFile(fileName) {
        this.elements.submittedAssignmentFileLink(fileName).click()
    }
}

export default new StudentLabPage();