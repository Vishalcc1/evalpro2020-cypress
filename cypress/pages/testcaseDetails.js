

class testcaseDetails{

    elements = {
        uploadFileButtonClk: ()=> cy.contains("Upload input in a file").click(),
        inputTestBox: () => cy.contains("Upload input from test box").click(),
        uploadFile:(_filePath,_mimeType,_fileName)=>cy.get('[name="inputFile"]').attachFile({ filePath : _filePath, mimeType : _mimeType , fileName :  _fileName}),
        uploadBtnClk: () => cy.get('[value="Check Output"]').click()
    }
    uploadTestFile(_filePath,_mimeType,_fileName)
    {
        console.log('Uploaded file: =======>', _filePath)
        this.elements.uploadFileButtonClk()
        this.elements.uploadFile(_filePath,_mimeType,_fileName)
        this.elements.uploadBtnClk()
        cy.contains('Not Found 404').should('exist')
    }
}
export default new testcaseDetails();
