// import parentPage from './parentPage'  //TODO shouldnt header be present on this page too?
import CourseSidebar from './courseSidebar'

/**
 * On student login.
 * Reach by: Clicking 'Run practice test' button on programming lab page.
 * Relative link: <baseUrl>/evaluate/practice/36
 */
class PracticeTestResultsPage { //extends parentPage {

    sidebar = new CourseSidebar();
    elements = {

        marksObtained: () => cy.get('h5 > span'),
        executionStatus : () => cy.get('tbody > tr > :nth-child(2)'), //.children()[1] // tbody > tr > :nth-child(2),
        // executionStatus : () => cy.contains('tbody').children()[0].children()[1] // tbody > tr > :nth-child(2)
        compareLink : () => cy.get('tbody > tr > :nth-child(8) > a'),
        testCaseTableRow: (testcaseName) => cy.contains('tr',testcaseName,{ timeout: 120000 })
    }

    
    getStatus() {
        // console.log("============================")
        // cy.log('=================')
        // cy.log(this.elements.executionStatus())
        // cy.log('---->', this.elements.executionStatus().contains('FAIL'))

        /*
            Kept this code comments for reference of what have been unsuccessfully tried. 
        */
        // return !this.elements.executionStatus().contains('FAIL');
        // return this.elements.executionStatus().should('have.text',' PASS ');
        return this.elements.executionStatus()
    }

    compareResults() {
        //TODO Explicitly visiting web page may break if link changes. 
        //So may be changing target while click link is still the best option,
        //as is done inside PracticeTestResultsPage.compareResults()

        //removeAttr is jQuery function. invoke() calls non-cypress functions
        //Avoiding opening comparison in new tab.
        return this.elements.compareLink().invoke('removeAttr', 'target onclick').click()
    }

    clickCompare(testName){
        this.elements.testCaseTableRow(testName).within(() => {
            cy.get('td').eq(7).within(()=>{
                cy.get('a').invoke('attr', 'href')
                    .then(($path) => {
                        cy.visit($path)
                        cy.go('back')
                    })
            })
        })
    }


    verifyTestcaseTableRow(name,staus,marks){
        this.elements.testCaseTableRow(name).within(() => {
            cy.get('td').should(($lis) => {
                expect($lis.eq(1)).to.contain(staus)
                expect($lis.eq(2)).to.contain(marks)
              })
        })
    }

    getMarks(){
        return this.elements.marksObtained()
    }

    outputIs(outputMessage) {
        return cy.contains(outputMessage)
    }
}

export default new PracticeTestResultsPage();