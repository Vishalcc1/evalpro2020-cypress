import CourseSidebar from './courseSidebar'

/**
 * On Instructor login.
 * Reach by: Clicking on labs using CourseSidebar.
 */

 class instructorLabPageAddSection {

    elements = {
        breadCrumb: () => cy.get('.breadcrumb'),
        addSectionbutton: () => cy.get('#addsectionlink'),
        sectionName : () => cy.get("#id_name"),
        sectionDescription : () => cy.get('.nicEdit-main').filter(':visible'),
        viewOnlySectionDescription : () => cy.get('#id_description'),
        sectionProgramType: () => cy.get("#id_program_type"),
        sectionExecutionCommandbox1Flags: () => cy.get('#id_execution_command_1'),
        sectionExecutionCommandbox2Files: () => cy.get('#id_execution_command_2'),
        sectionCompilerCommandbox1Flags: () => cy.get('#id_compiler_command_1'),
        sectionCompilerCommandbox2Files: () => cy.get('#id_compiler_command_2'),
        sectionCreate : () => cy.get(".btn").should('have.value', 'Create'),
        sectionInSectionTable : (sectionName) => cy.get("#TestCaseTable").contains('tr',sectionName),
        saveAndCloseButtonInSectionEditor : () => cy.get('[type="submit"]').should('have.value', 'Save & Close'),
        sectionAddTestcaseButton : () => cy.get('.glyphicon-plus'),
    }

    

    gotoAssignmentSectionListUsingBreadCrumb(assName){
        this.elements.breadCrumb().contains('a',assName).click()
    }

    clickAddSectionbutton(){
        this.elements.addSectionbutton().click()
    }

    typeSectionName(name){
        this.elements.sectionName().type(name)
    }

    typeSectionDescription(desc){
        this.elements.sectionDescription().click().type(desc)
    }

    selectSectionProgramType(ptype){
        this.elements.sectionProgramType().select(ptype).should('have.value',ptype)
    }

    typeSectionCommandbox1Flags(flags){
        cy.get("body").then($body => {
            if($body.find('#id_execution_command_1').length > 0){
                this.elements.sectionExecutionCommandbox1Flags().type(flags)
            }
            else{
                this.elements.sectionCompilerCommandbox1Flags().type(flags)
            }
        });    
    }

    typeSectionCommandbox2Files(files){
        cy.get("body").then($body => {
            if($body.find('#id_execution_command_1').length > 0){
                this.elements.sectionExecutionCommandbox2Files().type(files)
            }
            else{
                this.elements.sectionCompilerCommandbox2Files().type(files)
            }
        });    
    }
    

    clickSectionCreate(){
        this.elements.sectionCreate().click()
    }

    clickEditSectionInSectionTable(sectionName){
        this.elements.sectionInSectionTable(sectionName).contains('a','Edit').click()
    }

    clickViewSectionInSectionTable(sectionName){
        this.elements.sectionInSectionTable(sectionName).contains('a','View').click()
    }


    clickSaveAndCloseButtonInSectionEditor(){
        this.elements.saveAndCloseButtonInSectionEditor().click()
    }

    clickSectionAddTestcaseButton(){
        this.elements.sectionAddTestcaseButton().click()
    }

    verifySectionDetails(name,description,type,flags,files){
        this.elements.sectionName().should('have.value',name)
        this.elements.viewOnlySectionDescription().should('have.text',description)
        this.elements.sectionProgramType().within(() => {cy.get('option:selected').should('have.text', type)})
        //this.elements.sectionExecutionCommandbox1Flags().should('have.text',flags)

        cy.get("body").then($body => {
            if($body.find('#id_execution_command_1').length > 0){
                this.elements.sectionExecutionCommandbox2Files().should('have.value',files)
            }
            else{
                this.elements.sectionCompilerCommandbox2Files().should('have.value',files)
            }
        });    

    }

}

export default new instructorLabPageAddSection();