import CourseSidebar from './courseSidebar'

/**
 * On Instructor login.
 * Reach by: Clicking on labs using CourseSidebar.
 */

 class InstructorAddTestcase {
    elements = {
        testCaseNameTableRow: () => cy.contains('tr','Testcase Name:'),
        testCaseCommandLineArgs: () => cy.contains('label','Command Line Arguments:'),
        testCaseMarksTableRow: () => cy.contains('tr','Marks:'),
        testCaseFileOrBrowser: () => cy.contains('label','File upload:'),
        testCaseInputTableRow: () => cy.contains('tr','Input:'),
        testCaseOutputTableRow: () => cy.contains('tr','Output:'),
        testCaseDescriptionTableRow: () => cy.contains('tr','Description:'),
        testCasePartialMarking: () => cy.contains('label','Testcase level partial marking scheme ?'),
        testcaseSubmitButton : () => cy.get('[type="submit"]').should('have.value', 'Submit'),
        testcaseNextButton : () => cy.get('[type="submit"]').should('have.value', 'Next'),
        testInTestcaseTable : (testName) => cy.get("#TestCaseTable").contains('tr',testName),
    }

    typeTestCaseName(testcasename){
        this.elements.testCaseNameTableRow().within(() => {cy.get('input').type(testcasename)})
    }

    typeTestCaseCommandLineArgs(cmdLineArgs){
        this.elements.testCaseCommandLineArgs().type(cmdLineArgs)
    }

    typeTestCaseMarks(mrks){
        this.elements.testCaseMarksTableRow().within(() => {cy.get('input').type(mrks)})
    }

    checkTestCaseFileOrBrowser(checked){
        if(checked){
            this.elements.testCaseFileOrBrowser().click()
        }
    }

    typeTestCaseInput(input){
        this.elements.testCaseInputTableRow().within(() => {cy.get('textarea').type(input)})
    }

    typeTestCaseOutput(output){
        this.elements.testCaseOutputTableRow().within(() => {cy.get('textarea').type(output)})
    }

    typeTestCaseDescription(desc){
        this.elements.testCaseDescriptionTableRow().within(() => {cy.get('textarea').type(desc)})
    }

    checkTestCasePartialMarking(checked){
        if(checked){
            this.elements.testCasePartialMarking().click()
        }
    }

    clickTestcaseSubmitButton(){
        this.elements.testcaseSubmitButton().click()
    }

    clickTestcaseNextButton(){
        this.elements.testcaseNextButton().click()
    }

    clickEditInTestcaseTable(testName){
        this.elements.testInTestcaseTable(testName).contains('a','Edit').click()
    }

    verifyTestcase(testName,marks,input,output,desc){
        this.elements.testCaseNameTableRow().within(() => {cy.get('input').should('have.value',testName)})
        this.elements.testCaseMarksTableRow().within(() => {cy.get('input').should('have.value',marks)})
        this.elements.testCaseInputTableRow().within(() => {cy.get('textarea').should('have.text',input)})
        this.elements.testCaseOutputTableRow().within(() => {cy.get('textarea').should('have.text',output)})
        this.elements.testCaseDescriptionTableRow().within(() => {cy.get('textarea').should('have.text',desc)})
    }
}

export default new InstructorAddTestcase();