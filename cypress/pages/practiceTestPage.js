// import parentPage from './parentPage'  //TODO shouldnt header be present on this page too?
import CourseSidebar from './courseSidebar'

/**
 * On student login.
 * Reach by: Clicking 'Run practice test' button on programming lab page.
 * Relative link: <baseUrl>/evaluate/practice/36
 */
class PracticeTestPage extends parentPage {

    sidebar = new CourseSidebar();
    elements = {
        status : () => cy.contains('') // #accordion1pro > table > tbody > tr > td:nth-child(2)
    }

    getStatus() {
        return this.elements.status()
    }
}

export default new PracticeTestPage();