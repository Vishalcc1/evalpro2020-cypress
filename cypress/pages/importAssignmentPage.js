

class importAssignment{

    elements = {
        uploadFile:(_filePath,_mimeType,_fileName)=>cy.get('[id="id_assignment_archive"]').attachFile({ filePath : _filePath, mimeType : _mimeType , fileName :  _fileName}),
        uploadBtnClk: () => cy.get('[value="Create"]').click(),
        breadCrumb: () => cy.get('.breadcrumb')
    }
    uploadTestFile(_filePath,_mimeType,_fileName)
    {
        console.log('Uploaded file: =======>', _filePath)
        this.elements.uploadFile(_filePath,_mimeType,_fileName)
        this.elements.uploadBtnClk()
    }
    gotoAssignmentDescriptionUsingBreadCrumb(assName){
        this.elements.breadCrumb().contains('a',assName).click()
    }
}
export default new importAssignment();