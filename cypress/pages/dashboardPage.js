import ParentPage from './parentPage'

class dashboardPage extends ParentPage{
    elements = {

        dashboardLabel: () => cy.contains('My Dashboard'),
        // self: () => cy.contains('My Dashboard'),
        noCoursesAlert: () => cy.contains('No courses enrolled yet'),

        navDropdown: () => cy.get('#navbarDropdown'),
        logoutBtn: () => cy.contains('a','Sign Out'), //cy.get('a:contains("Sign Out")')
        unregisterYesBtn: () => cy.contains('button.btn-danger', 'Yes'),
        // TODO
        // unregisterYesBtn: (courseName) => (courseName) =>  cy.contains('div', courseName).next().contains('button.btn-danger', 'Yes') ,
        courseSection: (courseName) =>  cy.contains('div', courseName),
        unregisterBtn: (courseName) => cy.contains('div', courseName).next().next(),
        roleDropdown : () => cy.get("#modeDropDown"),
        roleBtn: (role) => cy.contains('a',role),
        createCourseBtn: () => cy.contains("Add Course"),
        rowBelowCourseName: (onlyName) => cy.get('[title="'+onlyName+'"]').next()
    }

    /**
     * Serves to see if the login is successful
     */
    checkIfItsDashboard() {
        this.elements.dashboardLabel().should('exist')
    }

    checkIfNoCourseIsEnrolled() {
        this.elements.noCoursesAlert().should('exist')
    }

    checkIfCourseIsEnrolled(courseName) {
        this.elements.courseSection(courseName).should('exist')
    }

    checkIfCourseIsNotEnrolled(courseName) {
        this.elements.courseSection(courseName).should('not.exist')
    }

    signOut() {
        this.clickNavbarDropdown()
        this.elements.logoutBtn().click()
    }

    clickUnregisterYes() {
        this.elements.unregisterYesBtn().click()
    }

    /**
     * The drop down containing log out button.
     */
    clickNavbarDropdown() {
        this.elements.navDropdown().click();
    }

    clickCourse(courseName) {
        this.elements.courseSection(courseName).click()
    }

    unregisterCourse(courseName) {
        this.checkIfCourseIsEnrolled(courseName)
        this.elements.unregisterBtn(courseName).click()
    }

    selectRole(role){
        this.elements.roleDropdown().click()
        this.elements.roleBtn(role).click()
    }

    clickCreateCourseBtn(){

        // this.elements.createCourseBtn().click({force: true})
        this.elements.createCourseBtn().click()
    }

    clickPublish_Unpublish_Course(onlyName){
        this.elements.rowBelowCourseName(onlyName).within(() => {
            cy.get('.db-publish').click()
        })
    }
}

export default new dashboardPage();