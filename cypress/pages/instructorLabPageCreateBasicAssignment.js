import CourseSidebar from './courseSidebar'

/**
 * On Instructor login.
 * Reach by: Clicking on labs using CourseSidebar.
 */

 class instructorLabPageCreateBasicAssignment {
    elements = {
        newButton : () => cy.contains('a','New'),
        importButton : () => cy.contains('a','Import'),
        assignmentName :  () => cy.get('#id_name'),
        assignmentDescription : () => cy.get('.nicEdit-main').filter(':visible'),
        assignmentDeadlineSubmission :  () => cy.get('#id_deadline'),
        assignmentDeadlineFreezing : () => cy.get('#id_freezing_deadline'),
        saveAndClose: () => cy.get('#id_submit'),
        programmingDetailsTab: () => cy.contains('a',' Programming Details'),
        isProgrammingAssignmentCheck: () => cy.get("#id_programmingCheck"),
        programmingLanguage: () =>  cy.get('select[id="id_program_language"]'),
        studentProgramFiles : () => cy.get("#id_student_program_files"),
        helperCodeFile : () =>cy.get('#id_helper_code'),
        viewOnlyHelperCodeFile : () => cy.get('#fileName_helper_code')
    }
    clickImportButton()
    {
        this.elements.importButton().click()
    }
    clickNewButtorn(){
        this.elements.newButton().click()
    }

    typeAssignmentName(assName){
        this.elements.assignmentName().type(assName)
    }

    typeAssignmentDescription(assDesc){
        this.elements.assignmentDescription().click().type(assDesc)
    }

    setAssignmentDeadlineSubmission(deadline){
        this.elements.assignmentDeadlineSubmission().invoke('attr', 'value', deadline)
    }

    setAssignmentDeadlineFreezing(deadline){
        this.elements.assignmentDeadlineFreezing().invoke('attr', 'value', deadline)
    }

    clickSaveAndClose(){
        this.elements.saveAndClose().click()
    }

    gotoProgrammingDetailsTab(){
        this.elements.programmingDetailsTab().click()
    }

    checkIsProgrammingAssignmentCheck(){
        this.elements.isProgrammingAssignmentCheck().click()
    }

    selectProgrammingLanguage(lang){
        this.elements.programmingLanguage().select(lang).should('have.value', lang)
    }

    typeStudentProgramFiles(fileName){
        this.elements.studentProgramFiles().invoke('attr', 'value', fileName)
    }

    uploadHelperCodeFile(filePath){
        this.elements.helperCodeFile().attachFile(filePath)
    }

    verifyBasicAssignmentDetails(assName,assDesc,sub_deadline,freeze_deadline){
        this.elements.assignmentName().should('have.value',assName)
        this.elements.assignmentDescription().should('have.html',assDesc)
        this.elements.assignmentDeadlineSubmission().should('have.value',sub_deadline)
        this.elements.assignmentDeadlineFreezing().should('have.value',freeze_deadline)
    }

    verifyProgrammingDetails(programmingLanguage,studentUploadFileNames,helperCodeFilePath){
        this.elements.programmingLanguage().within(() => {cy.get('option:selected').should('have.text', programmingLanguage)})
        this.elements.studentProgramFiles().should('have.value',studentUploadFileNames)
        this.elements.viewOnlyHelperCodeFile().should('contain.text',helperCodeFilePath.split('/').slice(-1).pop())
    }
}

export default new instructorLabPageCreateBasicAssignment();