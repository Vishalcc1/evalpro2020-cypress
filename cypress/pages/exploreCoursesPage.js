import ParentPage from './parentPage'

class exploreCourses extends ParentPage {
    elements = {
        courseBtn: (courseName) => cy.contains(courseName),
        enrollBtn: () => cy.contains('a','Enroll')
    }

    clickCourse(courseName) {
        this.elements.courseBtn(courseName).click()
    }

    clickEnrollBtn(){
        this.elements.enrollBtn().click()
    }
}

export default new exploreCourses();