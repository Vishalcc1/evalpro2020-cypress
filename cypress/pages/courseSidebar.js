/**
 * Contains sidebar links (e.g. Programming labs,  Subjective submission, 
 * Discussion forum etc.) on course page.
 */
 class CourseSidebar {
    elements = {
        programmingLabsBtn: () => cy.contains('Programming Labs',{ timeout:100000 }),

        peopleBtn: () => cy.contains('People')
    }

    clickProgrammingLabs() {
        this.elements.programmingLabsBtn().click({ timeout:100000 })
    }

    clickPeople(){
        this.elements.peopleBtn().click()
    }
}

export default CourseSidebar;