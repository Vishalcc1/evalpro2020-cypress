class ParentPage {
        
    commonElements = {
        bodhiTreeHomeBtn: () => cy.contains('a','BodhiTree'),
        exploreCoursesBtn: () => cy.contains('a','Explore'),
        myDashboardBtn: () => cy.contains('a','My Dashboard')
    }

    clicHome() {
        this.commonElements.bodhiTreeHomeBtn().click()
    }

    clickExploreCourses() {
        this.commonElements.exploreCoursesBtn().click()
    }

    clickMyDashboard() {
        this.commonElements.myDashboardBtn().click({force: true})
    }
}

export default ParentPage;