/**
 * This Page opened after clicking People
 */
 class peoplePage {
    elements = {
        approvedStudentsTable:() => cy.get('#approvedStudents'),
        approvedStudentHeading: () => cy.get('#approved-student-heading'),

        //Vishal code
        clickTaButton:() => cy.get('#pending-ta-heading'),
        enterTaDetails:()  => cy.get('[placeholder="Search existing users by Name/Email"]'),
        searchTaIntoCourse:() => cy.get('[value="submit"]'),
        addTaClickButton:() => cy.get('[data-reactid=".1.1.0.0.1.1.0.1.0.1.0.0"] > .col-md-offset-9 > .btn')

    }

    clickApprovedStudentHeading(){
        this.elements.approvedStudentHeading().click()
    }

    verifyStudentInApprovedStudentsTable(studentEmail){
        this.elements.approvedStudentsTable().within(() => {
            cy.contains('td',studentEmail).should('contain.text',studentEmail)
        })
    }

    //Vishal Code
    panelCollapseOpen(){
        this.elements.clickTaButton().click()
    }
    
    addTaintoCourse(taEmail){
        this.elements.enterTaDetails().type(taEmail)
        this.elements.searchTaIntoCourse().click()
        
        this.elements.addTaClickButton().click()
    }

}

export default new peoplePage;
