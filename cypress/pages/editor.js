import CourseSidebar from './courseSidebar'

/**
 * On Instructor login.
 * Reach by: Clicking on labs using CourseSidebar.
 */

 class Editor {
    elements = {
       //mainEditor : () => cy.get(".CodeMirror-activeline > .CodeMirror-line"), CodeMirror-line 
       mainEditor : () => cy.get(".CodeMirror-line").last(), 
       spanEditorLine: () => cy.get(".cm-variable"),
       submitAndTest : () => cy.get('[title="Click here to submit and run on visible testcases"]'),
       runTest: () => cy.get("#run-cpe").select("Run"),
       save: () => cy.get('[title="Click here to save the code. Each time you load editor saved code will be loaded"]'),
       fullScreen: () => cy.get('[title="Click here to enter/exit fullscreen"]'),
       testCaseTableRow: (testcaseName) => cy.contains('tr',testcaseName,{ timeout:100000 }),
       breadCrumb: () => cy.get('.breadcrumb'),
       mainContentBoxInCompare: () => cy.get('#contenthere'),
       errorBoxInCompare:() => cy.get('h5'),
       fullPracticeResultDIV : () => cy.get('#practice-results'),
       inputInRunTest : () => cy.get('#customInput')

    }

    
    typeTempEditor(code,executeSpecialCode){
        this.elements.mainEditor().type(code, {
            parseSpecialCharSequences: executeSpecialCode,
          })
    }

    


    typeEditor(code,executeSpecialCode){

        var latestBracketIndex = 0

        if(executeSpecialCode == false){
            for(var i=0; i<code.length;i++) 
            {
                if (code[i] === "(" || code[i] == "{" || code[i] == '[')
                {
                    this.elements.mainEditor().type(code.slice(latestBracketIndex,i+1)+' ', {
                        parseSpecialCharSequences: executeSpecialCode,
                    })

                    //cy.pause()

                    this.elements.mainEditor().type('{rightarrow}{backspace}{end}')

                    //cy.pause()

                    latestBracketIndex = i+1
                    
                }

            }
        }
        

        this.elements.mainEditor().type(code.slice(latestBracketIndex,code.length), {
            parseSpecialCharSequences: executeSpecialCode,
        })
    }

    
    selectActionSubmitAndPractice(){
        this.elements.submitAndTest().click()
    }
    

    selectActionRun(){
        this.elements.runTest()
    }

    clickSave(){
        this.elements.save().click()
    }

    clickFullScreen(){
        this.elements.fullScreen().click()
    }

    gotoAssignmentDescriptionUsingBreadCrumb(assName){
        this.elements.breadCrumb().contains('a',assName).click()
    }

    clickBrowserBackButtton(){
        cy.go('back')
    }

    clickCompare(testName){
        this.elements.testCaseTableRow(testName).within(() => {
            cy.get('td').eq(7).within(()=>{
                cy.get('a').invoke('attr', 'href')
                    .then(($path) => {
                        cy.visit($path)
                    })
            })
        })
    }

    verifyTestcaseTableRow(name,staus,marks){
        this.elements.testCaseTableRow(name).within(() => {
            cy.get('td').eq(1).should('have.html',staus)
            cy.get('td').eq(2).should('have.html',marks)
        })
    }

    verifyExpectedOutputRow(marks){
        // this.elements.testCaseTableRow("Expected Output").within(() => {
        //     cy.get('td').eq(3).should('have.html',marks)
        // })
        cy.get('tbody > tr').within(() =>{
            //cy.get('tr').within(() => {
                    cy.contains(marks)
            //})
        })
    }

    verifyTestcaseComapare(comment){
        if(comment != false)
            //this.elements.errorBoxInCompare().should('have.html',comment)
            cy.contains(comment)
    }



}

export default new Editor();
