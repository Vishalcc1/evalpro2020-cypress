# bodhitree2020 cypress framework

EvalPro2020 has features like "Ignore extra whitespaces", "Output contains numbers only.", "Order of numbers is important in output", "Enter value of Error rate" and different programming languages like "c" ,"C++" and "python". there is huge number of features so it require a rich Test BenchMark. because there is so many variable manual testing is nearly impossible to do for that purpose automated testing required. This is test we are **not test a exam mode** of EvalPro2020. this test is only test a Evaluation engine of bodhitree2020.

## Tech Stack
npm, npx, cypress,git

## Clone a repo

first we have to clone a repo in our system.

```javascript
> git clone https://gitlab.com/Vishalcc1/evalpro2020-cypress.git
# it will clone a repo but make sure that you install git already.
> cd evalpro2020-cypress
# change a dir and every other directory that we discusses later is relative to current directory
```

## Data Population

**Student:** Testing a EvalPro2020 we required a single student so do it manually using a admin Page or use sai kumar script to populate data. but make sure that student details is same as ` students.json` file.

```javascript
> cd cypress/fixtures/test_json/common
> cat students.json
"student1": {
    "username": "student_1",
    "password": "root",
    "first_name": "student1_first",
    "last_name": "student1_last",
    "email": "student1@user.com",
    "instructor": 0,
    "course_id": ""
  }
  # create single studenmt with details.
```


**Instructor:** Testing a EvalPro2020 we required a single instructor so do it manually using a admin Page or use sai kumar script to populate data. but make sure that student details is same as ` instructors.json` file.

```javascript
> cd cypress/fixtures/test_json/common
> cat instructors.json
"instructor1": {
    "username": "root",
    "password": "root",
    "first_name": "instructor1_first",
    "last_name": "instructor1_last",
    "email": "instructor1@instructor.com",
    "instructor": 1,
    "course_id": ""
  }
  # create single instructor with details.
```

Now change the bodhitree base url.
```javascript
> nano cypress.config.js
const { defineConfig } = require('cypress')
module.exports = defineConfig({
  ...
  ...
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config)
    },
    baseUrl: '{Bodhitree Base URL}',
  },
})
```

change the execute access of bash files.

```javascript
> sudo chmod 0777 createAssignment.sh testAssignment.sh
# Now we can execute a test
```

## Run Test

First execute `createAssignment.sh` script it will setup a course , enroll a first student into a course, create a 24 assignments.

```javascript
> ./createAssignment.sh
```

Run the test using `testAssignment.sh` file. it will upload 10 student code to every assignments. and generate a report in `cypress/reports` folder.

```javascript
>./testAssignment.sh

```

## Report 

It will generate a report in html format in 'cypress/reports` directory.

<p align="center">
  <img src="Screenshot from 2023-06-27 04-14-23.png" width="450" alt="accessibility text">
</p>
